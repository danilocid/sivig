<?php
Class Articulo{
    public $id;
    public $cod_interno;
    public $cod_barras;
    public $descripcion;
    public $precio_compra;
    public $precio_venta;
    public $stock;
    public $activo;
}
Class Articulos extends DB{
    public function GetArticulos(){
        $query = $this->connect()->prepare('SELECT * FROM articulos');
        $query->execute();
        $arrayarticulos = array();
        
        foreach ($query as $a) {
            $articulos = new Articulo();
            $articulos->id = $a[0];
            $articulos->cod_interno = $a[1];
            $articulos->cod_barras = $a[2];
            $articulos->descripcion = $a[3];
            $articulos->precio_compra = $a[4];
            $articulos->precio_venta = $a[5];
            $articulos->stock = $a[6];
            $articulos->activo = $a[7];
            

            array_push($arrayarticulos, $a);
        }
        return $arrayarticulos;
    }

    public function AgregarArticulos($articulos){
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE cod_interno = :cod_interno OR cod_barras = :cod_barras');
        $query->execute(['cod_interno' => $articulos[0]->cod_interno, 'cod_barras' => $articulos[0]->cod_barras]);
        if ($query->rowCount()) {
            echo '<script type="text/javascript">
            window.location="articulos.php?m=1";
            </script>';
        } else {
            try {
                $query = $this->connect()->prepare('INSERT INTO articulos VALUES(null, :cod_interno, :cod_barras, :descripcion,
                :precio_compra, :precio_venta, :stock, :activo)');
                $query->execute([
                    'cod_interno' => $articulos[0]->cod_interno,
                    'cod_barras' => $articulos[0]->cod_barras,
                    'descripcion' => $articulos[0]->descripcion,
                    'precio_compra' => $articulos[0]->precio_compra,
                    'precio_venta' => $articulos[0]->precio_venta,
                    'stock' => $articulos[0]->stock,
                    'activo' => $articulos[0]->activo
                ]);
                echo '<script type="text/javascript">
                    window.location="Articulos?m=1";
                  </script>';
            } catch (PDOException $e) {
                print_r('Error conenection: ' . $e->getCode());
               
            }
        }
    }
    public function GetArticulosPorId($id){
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE id = :id');
        $query->execute(['id' => $id]);
        $arrayarticulos = array();
        
        foreach ($query as $a) {
            $articulos = new Articulo();
            $articulos->id = $a[0];
            $articulos->cod_interno = $a[1];
            $articulos->cod_barras = $a[2];
            $articulos->descripcion = $a[3];
            $articulos->precio_compra = $a[4];
            $articulos->precio_venta = $a[5];
            $articulos->stock = $a[6];
            $articulos->activo = $a[7];
            

            array_push($arrayarticulos, $a);
        }
        return $arrayarticulos;
    }
    public function GetDescripcionArticuloPorId($id){
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE id = :id');
        $query->execute(['id' => $id]);
       
        
        foreach ($query as $a) {
            $descripcion = $a[3];
            
        }
        return $descripcion;
    }
    public function GetPrecioDeVentaArticuloPorId($id){
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE id = :id');
        $query->execute(['id' => $id]);
       
        
        foreach ($query as $a) {
            $precio = $a[5];
            
        }
        return $precio;
    }

    public function EditarArticulo($articulo){
        try {
            $query = $this->connect()->prepare('UPDATE articulos SET cod_interno = :cod_interno, cod_barras = :cod_barras,
            descripcion = :descripcion, precio_venta = :precio_venta,
            activo = :activo WHERE id = :id');
            $query->execute([
                'id' => $articulo[0]->id,
                'cod_interno' => $articulo[0]->cod_interno,
                'cod_barras' => $articulo[0]->cod_barras,
                'descripcion' => $articulo[0]->descripcion,
                'precio_venta' => $articulo[0]->precio_venta,
                'activo' => $articulo[0]->activo
            ]);
            echo '<script type="text/javascript">
                window.location="Articulos?m=3";
              </script>';
        } catch (PDOException $e) {
            print_r('Error conenection: ' . $e->getMessage());
           
        }
    }
    public function UpdateStockArticuloPorId($id, $cantidad, $precio_compra){
        try{
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE id = :id');
        $query->execute(['id' => $id]);
       
        
        foreach ($query as $a) {
            $stock = $a[6];
            
        }
        $stock = $stock + $cantidad;
        $query = $this->connect()->prepare('UPDATE articulos SET stock = :stock, precio_compra = :precio_compra WHERE id = :id');
        $query->execute(['id' => $id,
        'stock' => $stock,
        'precio_compra' => $precio_compra]);
    }catch (PDOException $e) {
        echo 'error al actualizar el stock del articulo';
        print_r('Error conenection: ' . $e->getMessage());
       
    }
    }
    public function UpdateStockArticuloVentaPorId($id, $cantidad){
        try{
        $query = $this->connect()->prepare('SELECT * FROM articulos WHERE id = :id');
        $query->execute(['id' => $id]);
       
        
        foreach ($query as $a) {
            $stock = $a[6];
            
        }
        $stock = $stock - $cantidad;
        $query = $this->connect()->prepare('UPDATE articulos SET stock = :stock WHERE id = :id');
        $query->execute(['id' => $id,
        'stock' => $stock
        ]);
    }catch (PDOException $e) {
        echo 'error al actualizar el stock del articulo';
        print_r('Error conenection: ' . $e->getMessage());
       
    }
    }
}


?>