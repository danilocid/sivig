<?php
Class AgregarRecepcion{
       
    public $proveedor;
    public $tipo_documento;
    public $documento;
    public $observaciones;
    public $unidades_total;
    public $monto_total;
    public $id;
    
}
Class ArticuloRecepcion{
    public $id;
    public $precio_compra;
    public $cantidad;
    public $total;
   
}
$titulo = 'Recepcion';
$idpagina = 5;
include 'includes/partials/header.php';
include 'includes/partials/menu.php';
include 'Controller/RecepcionesController.php';
include 'Controller/ProveedoresController.php';
include 'Controller/TipoDocumentoController.php';

if (isset($_SESSION['recepcion'])) {
   //echo 'Session cargada';
} else {
    $proveedor = $_POST['proveedor'];
    $tipo_documento = $_POST['tipo_documento'];
    $documento = $_POST['documento'];
    $observaciones = $_POST['observaciones'];
    $recepciones = new Recepciones();
    if ($recepciones->VerificaRecepcionExiste($proveedor, $documento)) {
      echo '<script type="text/javascript">
                    window.location="Recepcion?m=3";
                  </script>';
    }
    $recepcion = new AgregarRecepcion();
    $recepcionfrm = array();
    
    $recepcion->proveedor = $proveedor;
    $recepcion->tipo_documento = $tipo_documento;
    $recepcion->documento = $documento;
    $recepcion->observaciones = $observaciones;
    $recepcion->unidades_total = 0;
    $recepcion->monto_total = 0;
    $recepcion->usuario = $_SESSION['id'];
    $recepcion->id = 0;
    
    array_push($recepcionfrm, $recepcion);
    
    
    
    $_SESSION['recepcion'] = $recepcionfrm;
}


$proveedores = new Proveedores();
$tipos = new TiposDocumentos();
$articulos = new Articulos();
$monto_total = 0;
$total_articulos = 0;


?>

  <div class="container">
		<div class="row">
      <section>
          <h2>Recepcion de mercaderia</h2>
      </section>
      <div class="panel panel-default">
      <div class="panel-heading">
      <h3 class="panel-title">Recepcion de mercaderia</h3>
      </div>
      <div class="panel-body">
          <?php
         
          foreach ($_SESSION['recepcion'] as $r) {
              echo '<p>Proveedor: '.$proveedores->GetNombreProveedorPorRut($r->proveedor).'</p>';
              echo '<p>Documento: '.$tipos->GetTiposDocumentosPorId($r->tipo_documento).' N°: '.$r->documento.'</p>';
              
          }
          if (isset($_SESSION['articulo'])) {
            foreach ($_SESSION['articulo'] as $a) {
              $monto_total = $monto_total + $a->total;
              $total_articulos = $total_articulos + $a->cantidad;
              $_SESSION['recepcion'][0]->unidades_total = $total_articulos;
              $_SESSION['recepcion'][0]->monto_total = $monto_total;
            }
            echo '<p>Monto total: ' .number_format($monto_total, 0, ',', '.').'</p>';
            echo '<p>Total articulos: ' .number_format($total_articulos, 0, ',', '.').'</p>';
          }
          //echo $_SESSION['recepcion'][0]->unidades_total;
          //echo $_SESSION['recepcion'][0]->monto_total;
          ?>
</div>
      <div class="panel-body">
      <table id="tabla1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 20px">N°</th>
                  <th>Articulo</th>
                  <th>Precio de compra</th>
                  <th>Cantidad</th>
                  <th>Total</th>
                  
                </tr>
              </thead>
              <tbody>
              <?php
              $contador = 1;
              //print_r($_SESSION['articulo']);
              if (isset($_SESSION['articulo'])) {
                  //print_r($_SESSION['articulo']);
                foreach ($_SESSION['articulo'] as $a) {
                    echo '<tr>
                          <td>'.$contador.'</td>
                          <td>'.$articulos->GetDescripcionArticuloPorId($a->id).'</td>
                          <td> $'.number_format($a->precio_compra, 0, ',', '.').'</td>
                          
                          <td>'.$a->cantidad.'</td>
                          <td> $'.number_format($a->total, 0, ',', '.').'</td>';
                          
                          
                         
                                        
                          echo '</tr>
                          ';
                      $contador++;
                    }
                    
              }
              ?>
              
            </tbody>
            
              
            </table>
            <br>
              <div class="pull-right">
            <button type="button" class="btn  btn-lg btn-info" data-toggle="modal" data-target="#modal-default">
                    Agregar articulo
                  </button>
                  </div>
                  <div class="pull-left">
            
                   <a href="AgregarRecepciones.php" class="btn  btn-lg btn-info"> Terminar recepcion</a>
                
                  </div>
                  <br>
                  <br>
                  <br>
                  <div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Agregar articulo</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="AgregarArticuloRecepcion" method="POST">        
                        <div class ="form-group">
                          <label>Articulo</label>
                          <select id="articulo" name="articulo" class="form-control select2" style="width: 100%;">
                          <option value=0>Seleccionar articulo</option>
                          <?php
                             
                              $articulo = $articulos->GetArticulos();
                              foreach ($articulo as $a) {
                                echo '<option value="'.$a['id'].'">'.$a['cod_interno'].'</option>';
                              }
                          ?>                
                          </select>
                        </div>
                        
                        <div class ="form-group">
                          <label>Cantidad</label>
                          <input required type="number" id="cantidad" name="cantidad" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Precio de compra</label>
                          <input id="precio_compra" name="precio_compra" required type="text" class="form-control input-sm">
                        </div>
                        
                        
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar articulo</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
              
        </div>
        <div class="modal fade" id="cliente-existe">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Articulo ya existe</h4>
              </div>
              <div class="modal-body">
                
                    <p>Articulo ingresado ya existe en la base de datos</p>
                
              </div>
              <div class="modal-footer">
                
                <a href="Articulos" class="btn btn-default pull-left">Cerrar</a>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          </div>
          <div class="modal fade" id="cliente-editado">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Articulo modificado</h4>
              </div>
              <div class="modal-body">
                
                    <p>Datos del articulo actualizados</p>
                
              </div>
              <div class="modal-footer">
                
                <a href="Articulos" class="btn btn-default pull-left">Cerrar</a>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          
          </div>
      </div>
      </div></div>
      </div>
      </div>
     
      
      
  
  <?php
  if (isset($_GET['m'])) {
    if($_GET['m'] == 1){
      echo '<script type="text/javascript">';
      echo "$('#cliente-existe').modal('toggle');
      
    </script>";
    }
    if($_GET['m'] == 2){
      echo '<script type="text/javascript">';
      echo "$('#cliente-editado').modal('toggle');
      
    </script>";
    }
  }
    
  include 'includes/partials/footer.php';
  
  ?>

