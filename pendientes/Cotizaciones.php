<?php
$titulo = 'Cotizaciones';
$idpagina = 2;
include 'includes/partials/header.php';
include 'includes/partials/menu.php';
include 'Controller/ClientesController.php';
include 'Controller/TipoDocumentoController.php';
include 'Controller/CotizacionesController.php';

$clientes = new Cliente();
$tipo_documento = new TiposDocumentos();
if (isset($_SESSION['articulo'])) {
  unset($_SESSION['articulo']);
}
?>
  <div class="container">
		<div class="row">
      <section>
          <h2>Cotizaciones</h2>
      </section>
      <div class="panel panel-default">
      <div class="panel-heading">
      <h3 class="panel-title">Control de cotizaciones</h3>
      </div>
      <div class="panel-body">
      <table id="tabla1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 60px">ID</th>
                  <th>Cliente</th>
                  <th>Monto</th>
                  <th>Fecha</th>
                  <th>Observaciones</th>
                  <th>Estado</th>
                  <th style="width: 60px">Ver detalle</th>
                </tr>
              </thead>
              <tbody>
              <?php
                  $cotizaciones = new Cotizaciones();
                  $cotizacion = $cotizaciones->GetCotizaciones();
                  
                  foreach ($cotizacion as $c) {
                   echo' <tr>
                      <td>'. $v['id'].'</td>  
                      <td>'. $clientes->GetNombreClientePorRut($v['cliente']).'</td>
                      <td>'. $tipo_documento->GetTiposDocumentosPorId($v['tipo_documento']).'</td>
                      <td>'. $v['documento'].'</td>
                      <td>$'.number_format($v['monto'], 0, ',', '.').'</td>
                      <td>'. $v['fecha'].'</td>
                      <td>
                    <form action="VerVenta" method="POST">
                    <input type="hidden" name="id" value="'.$v['id'].'">
                    <button type="submit" class="btn btn-block btn-primary btn-sm" name="Ver detalle" value="Ver detalle">Ver detalle</button>
                    </form>
                    </td>
                    </tr>';
                    
                  }
              ?>
            </tbody>
            
              
            </table>
            <br> 
              <div class="pull-right">
              <a href="AgregarCotizacion" class="btn  btn-lg btn-info"> Agregar cotizacion</a>
                  </div>
                  <br>
                  <br>
                  <br>
                  
       
          
          
          </div>
      </div>
     
      </div>
      </div>
     
      
      
  
  <?php
  
    
  include 'includes/partials/footer.php';
  
  ?>

