<?php
$titulo = 'Editar Usuario - SIVIG';
$idpagina = 9;
include 'includes/partials/header.php';
include 'includes/partials/menu.php';
include 'Controller/UsuariosController.php'

?>

<div class="container">
		<div class="row">
      <section>
          <h2>Modificacion de usuario</h2>
      </section>
      <div class="panel panel-default">
      <div class="panel-heading">
      <h3 class="panel-title">Administracion de usuarios</h3>
      </div>
      <div class="panel-body">
      <div class="col-md-6">
        
        <?php
    $id = $_POST["id"];
    

    $usuario = new Usuario();
      $usuarios = $usuario->GetUsuarioPorId($id);
      echo '<form role="form" action="ModificarUsuario.php" method="POST">';
      
      foreach ($usuarios as $u) {
             echo '<div class ="form-group">
                    <label>Nombre</label>
                    <input id="Nombre" name="Nombre" required type="text" class="form-control" value="' .$u['Nombre']. '">
                    </div>
                    <div class ="form-group">
                    <label>Apellidos</label>
                    <input required type="text" id="apellidos" name="apellidos" class="form-control" value="'.$u['Apellidos'].'">
                    </div>
                    <div class ="form-group">
                    <label>Usuario</label>
                    <input id="user" name="user" required type="text" class="form-control" value="'.$u['User'].'">
                    </div>
                    <div class ="form-group">
                    <label>Contraseña</label>
                    <input id="password" name="password" required type="text" class="form-control" value="'.$u['password'].'">
                    </div>
                    <input type="hidden" name="id" value="'.$u['IdUsuario'].'">
                    <div class="form-group">
                    <label>Activo</label>
                    
                  
                    ';
                    echo '<select id="activo" name="activo" class="form-control">';
                    if ($u['Activo'] == 1) {
                      
                      echo '<option>Activo</option>
                      <option>Inactivo</option>
                                              
                    </select>
                    </div>';
                    } else {
                     echo '<option>Activo</option>
                      <option selected>Inactivo</option>
                     
                    </select>
                    </div>';
                    }
                    
            }
    ?>
    
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
            Guardar cambios
          </button>

          <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modificar usuario</h4>
          </div>
          <div class="modal-body">
            <p>Seguro que quiere guardar los cambios?&hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    </form>
    </div>
      </div>
      </div>
    </div>
  </div>
  

  

  <?php
  include 'includes/partials/footer.php';
  ?>