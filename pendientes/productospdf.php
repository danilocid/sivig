<?php
require('../PDF/fpdf.php');
include '../Model/dbConection.php';
require('../Controller/ArticulosController.php');

$articulo = new Articulos();
$articulos = $articulo->GetArticulos();
            
              


class PDF extends FPDF
{
	
// Cabecera de p�gina
function Header()
{
	$titulo = "Resumen de articulos";
	// Logo
	$this->Image('../images/logo-web.jpg',10,8,33);
	// Arial bold 15
	$this->SetFont('Arial','B',15);
	// Movernos a la derecha
	$this->Cell(80);
	// T�tulo
	
	
	$this->Cell(30,10,$titulo,0,2,'C');
	// Salto de l�nea
	$this->Ln(20);
}

// Pie de p�gina
function Footer()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// N�mero de p�gina
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creaci�n del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',8);

foreach ($articulos as $c) {
	$pdf->Cell(9, 8, $c['id'], 1, 0, 'C');
	$pdf->Cell(23,8,$c['cod_interno'], 1, 0, 'L');
	$pdf->Cell(90,8,$c['descripcion'], 1, 0, 'L');
	$pdf->Cell(15,8,$c['precio_compra'], 1, 0, 'L');
	$pdf->Cell(15,8,$c['precio_venta'], 1, 0, 'L');
	$pdf->Cell(15,8,$c['stock'], 1, 1, 'L');
}
$pdf->Output();
?>
