<?php
Class ArticuloVenta{
  public $id;
  public $precio_venta;
  public $cantidad;
  public $total;
 
}

$titulo = 'Agregar recepcion';
$idpagina = 8;
include 'includes/partials/header.php';
include 'includes/partials/menu.php';
include 'Controller/TipoDocumentoController.php';
include 'Controller/ArticulosController.php';
include 'Controller/ClientesController.php';
include 'Controller/ComunasController.php';
include 'Controller/RegionesController.php';
include 'Controller/MediosDePagoController.php';



$tipos = new TiposDocumentos();
$articulos = new Articulos();
$clientes = new Cliente();
$medios_de_pago = new MediosDePago();
$monto_total = 0;
$total_articulos = 0;


?>
<script language="javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		
		<script language="javascript">
			$(document).ready(function(){
				$("#region").change(function () {
 
					$('#provincia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#region option:selected").each(function () {
						idregion = $(this).val();
						$.post("Controller/GetProvincias.php", { idregion: idregion }, function(data){
              $("#provincia").html(data);
             
						});            
					});
				})
        $("#provincia").change(function () {
 
          $('#comuna').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
 
          $("#provincia option:selected").each(function () {
          idprovincia = $(this).val();
           $.post("Controller/GetComunas.php", { idprovincia: idprovincia }, function(data){
            $("#comuna").html(data);
           
   });            
 });
})
			});
		</script>
    
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Agregar recepcion</h1>
            </div>
            
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Carrito</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
          
            <!-- Inicio contenido -->
            
            
            <table id="tabla2" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 20px">Id</th>
                  <th>Codigo interno</th>
                  <th>Codigo de barras</th>
                  <th>Descripcion</th>
                  <th>Stock</th>
                  <th>Precio de venta</th>
                  <th style="width: 60px">Cantidad</th>
                  <th style="width: 60px">Agregar</th>
                </tr>
              </thead>
              <tbody>
              <?php

               if (isset($_SESSION['articulo'])) {
                 
                foreach ($_SESSION['articulo'] as $a) {
                $articuloventa = $articulos->GetArticulosPorId($a->id);
                
                echo '<tr>
                    <td>'.$a->id.'</td>
                    <td>'.$articuloventa[0]['cod_interno'].'</td>
                    
                    <td>'.$articuloventa[0]['cod_barras'].'</td>
                    <td>'.$articuloventa[0]['descripcion'].'</td>
                    <td>'.$articuloventa[0]['stock'].'</td>';
                    echo '<td>
                    <form action="EliminarArticuloVenta" method="POST">
                    <input class="form-control input-sm" type="number" name="precio_venta" required value="'.$a->precio_venta.'">
                    </td>';
                    echo '<td>
                    <input class="form-control input-sm" type="number" name="cantidad" required value="'.$a->cantidad.'">
                    </td>';
                    echo '<td>
                    
                    <input type="hidden" name="id" value="'.$a->id.'">
                    
                    <button type="submit" class="btn btn-block btn-primary btn-sm" name="Eliminar" value="Eliminar">Eliminar</button>
                    </form>
                    </td>';
                                  
                    echo '</tr>
                    ';
                
              }
            }
              ?>
            </tbody>
            
              
            </table>
                
                
            <!-- Fin contenido -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
         Carrito
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

         <!-- Default box -->
         <div class="card">
          <div class="card-header">
            <h3 class="card-title">Documento y cliente</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
          
            <!-- Inicio contenido -->
            
            
            <form action="AgregarVentas" method="POST">
           <div class="col-md-6">
           <div class ="form-group">
                          <label>Tipo documento</label>
                          <select id="tipo_documento" name="tipo_documento" class="form-control select2">
                          <?php
                             
                              $tipo = $tipos->GetTiposDocumentos();
                              foreach ($tipo as $t) {
                                echo '<option value="'.$t['id'].'">'.$t['tipo'].'</option>';
                              }
                          ?>                
                    </select>
                    </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label>Numero documento</label>
                  <input id="numero_documento" name="numero_documento" required type="number" class="form-control input-sm">
              </div>
            </div>
            <div class="col-md-6">
            <div class ="form-group">
                          <label>Medio de pago</label>
                          <select id="mediopago" name="mediopago" class="form-control select2">
                          <?php
                             
                              $medio = $medios_de_pago->GetMediosDePago();
                              foreach ($medio as $m) {
                                echo '<option value="'.$m['id'].'">'.$m['medio_de_pago'].'</option>';
                              }
                          ?>                
                    </select>
                    </div>
            </div>
            <div class="col-md-6">
              <div class ="form-group">
                    <label>Cliente</label>
                    <select id="cliente" name="cliente" class="form-control select2">
                    <?php
                        
                        $cliente = $clientes->GetClientes();
                        foreach ($cliente as $t) {
                          echo '<option value="'.$t[0].'">'.$t[1].' '.$t[2].' ('.$t[0].')</option>';
                          
                        }
                    ?>                
                    </select>
                    </div>
            </div>
            
            <?php  
       
          if (isset($_SESSION['articulo'])) {
            foreach ($_SESSION['articulo'] as $a) {
              $monto_total = $monto_total + $a->total;
              $total_articulos = $total_articulos + $a->cantidad;
              
            }
            echo '<div class="col-md-6">
              <div class="form-group">
                  <label>Monto total</label>
                  <input id="monto_total" name="monto_total" disabled type="text" class="form-control input-sm" value="$'.number_format($monto_total, 0, ',', '.').'">
                  <input id="monto_total" name="monto_total" type="hidden" class="form-control input-sm" value="'.$monto_total.'">
              </div>
            </div>';
            echo '<div class="col-md-6">
              <div class="form-group">
                  <label>Total articulos</label>
                  <input id="total_articulos" name="total_articulos" disabled type="text" class="form-control input-sm" value="'.number_format($total_articulos, 0, ',', '.').'">
              </div>
            </div>
            <div class="col-md-3">
                    <div class="form-group pull-right">
            <button type="submit" class="btn  btn-md btn-warning">
                    Finalizar venta
            </button>
            </div>
            </div>';
           
           
          }
          
          ?>
            
            
            
            
            
          </form>
                  <div class="col-md-2">
           <div class="pull-right">
           <button type="button" class="btn  btn-lg btn-info" data-toggle="modal" data-target="#modal-default">
                    Agregar cliente
                  </button>
                  </div>  
                  <div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    
                    <h4 class="modal-title">Agregar cliente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="AgregarCliente" method="POST">        
                        <div class ="form-group">
                          <label>Nombre o Razon Social</label>
                          <input id="nombre" name="nombre" required type="text" class="form-control input-sm">
                          <input id="enlace" name="enlace" type="hidden"  value="clientes" class="form-control input-sm">
                        </div>
                       
                        <div class ="form-group">
                          <label>RUT</label>
                          <input id="rut" name="rut" required oninput="checkRut(this)" type="text" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Giro</label>
                          <input id="giro" name="giro" required type="text" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Direccion</label>
                          <input id="direccion" name="direccion" required type="text" class="form-control input-sm">
                        </div>
                        
                        <div class="form-group">
                          <label>Region</label>
                          <select id="region" name="region" required class="form-control input-sm">
                          <option value="">Selecciona</option>
                            <?php
                            $region = new Regiones();
                            $regiones = $region->GetRegiones();
                            foreach ($regiones as $r) {
                              echo '<option value=' . $r->id . '>' . $r->region . '</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Provincia</label>
                          <select id="provincia" name="provincia" required class="form-control input-sm">
                            
                          </select>
                        </div>  
                        <div class="form-group">
                          <label>Comuna</label>
                          <select id="comuna" name="comuna" required class="form-control input-sm">
                            
                          </select>
                        </div> 
                        <div class ="form-group">
                          <label>Telefno</label>
                          <input id="telefono" name="telefono" required type="number" min="111111111" max="999999999" class="form-control input-sm">
                        </div>     
                        <div class ="form-group">
                          <label>Mail</label>
                          <input id="mail" name="mail" required type="email" class="form-control input-sm">
                        </div>          
                        
                          </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar cliente</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
                  </div>
                  </div>
                
            <!-- Fin contenido -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
          Documento y cliente
          </div>
          <!-- /.card-footer-->
        </div>

         <!-- Default box -->
         <div class="card">
          <div class="card-header">
            <h3 class="card-title">Articulos</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
          
            <!-- Inicio contenido -->
            
            
            <table id="tabla1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 20px">Id</th>
                  <th>Codigo interno</th>
                  <th>Codigo de barras</th>
                  <th>Descripcion</th>
                  <th>Stock</th>
                  <th>Precio de venta</th>
                  <th style="width: 60px">Cantidad</th>
                  <th style="width: 60px">Agregar</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $articulo = new Articulos();
              $articulos = $articulo->GetArticulos();
            
              foreach ($articulos as $c) {
              echo '<tr>
                    <td>'.$c['id'].'</td>
                    <td>'.$c['cod_interno'].'</td>
                    
                    <td>'.$c['cod_barras'].'</td>
                    <td>'.$c['descripcion'].'</td>
                    <td>'.$c['stock'].'</td>';
                    echo '<td>
                    <form action="AgregarArticuloVenta" method="POST">
                    <input class="form-control input-sm" type="number" name="precio_venta" required value="'.$c['precio_venta'].'">
                    </td>';
                    echo '<td>
                    <input class="form-control input-sm" type="number" name="cantidad" min="1" pattern="^[0-9]+" required value="">
                    </td>';
                    echo '<td>
                    
                    <input type="hidden" name="id" value="'.$c['id'].'">
                    
                    <button type="submit" class="btn btn-block btn-primary btn-sm" name="Agregar" value="Agregar">Agregar</button>
                    </form>
                    </td>';
                                  
                    echo '</tr>
                    ';
                
              }
              ?>
            </tbody>
            
              
            </table>
                
                
            <!-- Fin contenido -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
          Articulos
          </div>
          <!-- /.card-footer-->
        </div>
  
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  

      
        
          
           

      
      
            
                  
      </div>
      
      
      </div>
</div>

</div>

<div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Agregar cliente</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="AgregarCliente" method="POST">        
                        <div class ="form-group">
                          <label>Nombre o Razon Social</label>
                          <input id="nombre" name="nombre" required type="text" class="form-control input-sm">
                          <input id="enlace" name="enlace" type="hidden"  value="AgregarVenta" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Apellidos</label>
                          <input required type="text" id="apellidos" name="apellidos" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>RUT</label>
                          <input id="rut" name="rut" required type="text" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Giro</label>
                          <input id="giro" name="giro" required type="text" class="form-control input-sm">
                        </div>
                        <div class ="form-group">
                          <label>Direccion</label>
                          <input id="direccion" name="direccion" required type="text" class="form-control input-sm">
                        </div>
                        
                        <div class="form-group">
                          <label>Region</label>
                          <select id="region" name="region" class="form-control input-sm">
                            <?php
                            $region = new Regiones();
                            $regiones = $region->GetRegiones();
                            foreach ($regiones as $r) {
                              echo '<option value=' . $r->id . '>' . $r->region . '</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Provincia</label>
                          <select id="provincia" name="provincia" class="form-control input-sm">
                            
                          </select>
                        </div>  
                        <div class="form-group">
                          <label>Comuna</label>
                          <select id="comuna" name="comuna" class="form-control input-sm">
                            <option>Activo</option>
                            <option>Inactivo</option>
                          </select>
                        </div> 
                        <div class ="form-group">
                          <label>Telefno</label>
                          <input id="telefono" name="telefono" required type="number" class="form-control input-sm">
                        </div>     
                        <div class ="form-group">
                          <label>Mail</label>
                          <input id="mail" name="mail" required type="mail" class="form-control input-sm">
                        </div>          
                        
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar cliente</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
              
        </div>  
        <div class="modal fade" id="cliente-existe">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cliente existe</h4>
              </div>
              <div class="modal-body">
                
                    <p>Cliente ingresado ya existe en la base de datos</p>
                
              </div>
              <div class="modal-footer">
                
                <a href="AgregarVenta" class="btn btn-default pull-left">Cerrar</a>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
  
  <?php
  if (isset($_GET['m'])) {
    if($_GET['m'] == 1){
      echo '<script type="text/javascript">';
      echo "$('#cliente-existe').modal('toggle');
      
    </script>";
    }
    if($_GET['m'] == 2){
      echo '<script type="text/javascript">';
      echo "$('#cliente-editado').modal('toggle');
      
    </script>";
    }
  }
    
  include 'includes/partials/footer.php';
  
  ?>

