<?php
session_start();
if(isset($_SESSION['user'])){
    //echo $_SESSION['id'];
}else{
    echo '<script type="text/javascript">
                window.location="login";
			  </script>';
	exit;
    //echo $_SESSION['user'];
}
?>
<!DOCTYPE html>

<html>
<head>

	<meta charset="utf-8">
	<title><?php echo $titulo; ?></title>
	<link rel="stylesheet" href="includes/css/bootstrap.min.css">
    <link rel="stylesheet" href="includes/css/font-awesome.min.css">
  	<link rel="stylesheet" href="includes/css/ionicons.min.css">
    <link rel="stylesheet" href="includes/css/dataTables.bootstrap.min.css">
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="includes/dist/bootswatch/cosmo/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="includes/dist/jquery-ui/jquery-ui.min.css"/>
	
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="includes/css/bootstrap-datepicker.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="includes/css/select2.min.css">
	
	<style type="text/css">
		html {
			overflow: auto;
		}
	</style>
</head>

<body>
	<div class="wrapper">
		<div class="topbar">
			<div class="container">
						<div class="navbar-right" style="margin:0">
					<a><?php echo $_SESSION['nombre']; ?></a>|<a href="Controller/LogoutController.php">Salir</a></div>
			</div>
    </div>
