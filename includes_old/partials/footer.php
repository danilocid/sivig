</div>
<div id="footer">
		<div class="jumbotron">
        <div class="pull-right hidden-xs">
      <b>Version</b> Pre-Alpha</div>
    Algunos derechos reservados</div>
</div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="includes/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="includes/js/bootstrap.min.js"></script>
<script src="includes/js/jquery.dataTables.min.js"></script>
<script src="includes/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="includes/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="includes/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="includes/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="includes/js/demo.js"></script>
<!-- Select2 -->
<script src="includes/js/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<script src="includes/js/bootstrap-datepicker.min.js"></script>
<script src="includes/js/bootstrap-datepicker.es.min.js"></script>
<!-- Select2 -->
<script src="Includes/plugins/select2/js/select2.full.min.js"></script>
<script>
  $(function () {
   
    $('#tabla1').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
    $('#tabla2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
    $('.select2').select2()
    $('#datepicker').datepicker({
      format: "yyyy/mm/dd",
        clearBtn: true,
        language: "es",
        autoclose: true,
        keyboardNavigation: false,
        todayHighlight: true
    })
  })
  function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
    
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}
</script>


</body>
</html>
