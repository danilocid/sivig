<div class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a class="navbar-brand hidden-sm" href="./">SIVIG</a>
				</div>

				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
          <?php
        require './Controller/MenuController.php';

        $menu = new Menu();
        $datos = $menu->getpaginas($_SESSION['id']);
        foreach ($datos as $n) {
          echo '
          
          <li class="">
					<a href="'. $n->EnlacePagina.'" title="'. $n->NombrePagina.'" class="menu-icon">
					<img src="images/menubar/'.$n->ImagenPagina.'" border="0" alt="Module Icon"/><br/>'. $n->NombrePagina.'</a>
					</li>';
	  }
	  if($idpagina != 0){
        if ($menu->GetPermisoPagina($_SESSION['id'],$idpagina)) {
          echo '<script type="text/javascript">
          window.location="index";
          </script>';
        }
	}
      

        ?>
					
					
					</ul>
				</div>
			</div>
    </div>
    

 