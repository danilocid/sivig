<?php
$titulo = 'Recepcion';
$idpagina = 5;
include 'includes/partials/header.php';
include 'includes/partials/menu.php';
include 'Controller/RecepcionesController.php';
include 'Controller/ProveedoresController.php';
include 'Controller/TipoDocumentoController.php';
$proveedores = new Proveedores();
$tipos = new TiposDocumentos();

if(isset($_SESSION['recepcion'])){
  unset($_SESSION['recepcion']);
}
if (isset($_SESSION['articulo'])) {
  unset($_SESSION['articulo']);
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Recepcion de mercadiera</h1>
            </div>
            
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Recepciones</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
          
            <!-- Inicio contenido -->
            
            
            <table id="tabla2" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 20px">Id</th>
                  <th>Proveedor</th>
                  <th>Tipo Documento</th>
                  <th>Documento</th>
                  <th>Monto</th>
                  <th>Fecha</th>
                  <th style="width: 80px">Ver detalle</th>
                 
                </tr>
              </thead>
              <tbody>
              <?php
              $recepcion = new Recepciones();
              $recepciones = $recepcion->GetRecepciones();
            
              foreach ($recepciones as $c) {
              echo '<tr>
                    <td>'.$c['id'].'</td>
                    <td>'.$proveedores->GetNombreProveedorPorRut($c['proveedor']).'</td>
                    <td>'.$tipos->GetTiposDocumentosPorId($c['tipo_documento']).'</td>
                    <td>'.$c['documento'].'</td>
                    <td>$'.number_format($c['monto_total'], 0, ',', '.').'</td>
                    <td>'.$c['fecha'].'</td>';
                    
                    echo '<td>
                    <form action="VerRecepcion" method="POST">
                    <input type="hidden" name="id" value="'.$c['id'].'">
                    <button type="submit" class="btn btn-primary btn-sm" name="Ver" value="Ver">Ver detalle</button>
                    </form>
                    </td>';
                    
                                  
                    echo '</tr>
                    ';
                
              }
              ?>
            </tbody>
            
              
            </table>
            
                
            <!-- Fin contenido -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
Recepciones          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
  
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


  
         
                  <br>
                  <br>
                  <br>
                  
              
      
      
      
  
  <?php
      
  include 'includes/partials/footer.php';
  
  ?>

